package interfaz_desde_cero;
import javax.swing.*;
import java.awt.event.*;

public class Practica_JButton extends Jframe implements ActionListener {
	
	JButton Boton1;//al declarar no es necesario inicializar
	public Practica_JButton(){
		
		setLayout(null);
		Boton1=new JButton("Cerrar");
		Boton1.setBounds(50,100,100,40);
		add(Boton1);
		Boton1.addActionListener(this);// indicamos a la clase de Action listener que espere a que el boton sea pulsado
		
	
	}

	public void actionPerformed(ActionEvent cerrar) {//aqui se va a capturar el evento
		
		if(cerrar.getSource() == Boton1 ){//getSource sirve para meter un evento en un objeto en nuestro caso es Boton1
			System.exit(0);
			
		}
		
	
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
