package interfaz_desde_cero;
import javax.swing.*;

public class Practica_JLabel extends JFrame{
	
	private JLabel Etiqueta1;//lo ponemos privado para que no se modifique en otras clases
	private JLabel Etiqueta2;
	
	//Aqui se crea el constructor del Label
	//los construcctores siempre se llaman igual que la clase
	public Practica_JLabel() {
		setLayout(null);//esto es un metodo que indica que se van a colocar elementos con coordenadas
		Etiqueta1=new JLabel("Hola Mundo Soy Miguel");
		Etiqueta1.setBounds(10,20,300,30);
		add(Etiqueta1);
		
		Etiqueta2=new JLabel("Version 1.0 de la interfaz");
		Etiqueta2.setBounds(10,100,100,30);
		add(Etiqueta2);
	}
	
	

	public static void main(String[] args) {
		
		 Practica_JLabel Ventana =new Practica_JLabel();
		 Ventana.setBounds(0,0,300,400);
		 Ventana.setResizable(true);
		 Ventana.setVisible(true);
		 Ventana.setLocationRelativeTo(null);//indica que cuando arranque la interfaz aparezca en el centro de la pantalla

		
	}
	

}
