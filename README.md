# Proyecto Entornos de Desarrollo
---

## Intención del Proyecto

Utilizar la metodología agil de trabajo Scrum cumpliendo con todas
las especificaciones de Scrum.


## Grupo

En el pryecto participamos cuatro personas.

- Dragos C. Butnariu
- Miguel Angel Cantarero
- Adrian Carrasco
- Maria Pardillo


## Proyecto

Aplicación grafica que te permita seleccionar una clase y mediante
un boton generar un aleatorio para que saque un nombre, y posteriormente
hacer una pregunta a la persona con la intencion de calificar de forma
positiva o negativa, mediate otro boton.

Tambien tendrá que generar graficos de la clase/alumno.

## Codigo Fuente

Como lenguaje de programación hemos elegido Java con la libreria
grafica Java Swing.
