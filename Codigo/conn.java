package conn_bbdd;

import java.sql.*;

public class conn {

    public static void main ( String[] args ) {
        // Datos de la conexión, en mi maquina
        String connUrl = "jdbc:mysql://localhost:3306/scrum?serverTimezone=UTC", 
                us = "admin", // Modificar el usuario y password segun el usuario de cada uno en mysql.
                pass = "admin123";
        Connection conexion;
        ResultSet res;
        conexion = Conexiones.conectarDB ( connUrl, us, pass );
        /* 
         * Metodos con la BBDD:
         * 
         * Conexiones.conectarDB();
         * Conexiones.consultaClase();
         * Conexiones.consultaAlumno();
         * Conexiones.puntoPositivo();
         * Conexiones.puntoNegativo();
         * Conexiones.crearAlumno();
         * Conexiones.crearClase();
         * 
         */

        
        
        
        /*
         * try { while ( res.next () ) { System.out.println ( res.getInt (
         * "id_alumno" ) + " " + res.getString ( "nombre" ) ); } } catch (
         * SQLException e ) { e.printStackTrace (); }
         */

        /*
         * Connection con = null; // Variable de conexion. int id_al; Scanner sc
         * = new Scanner (System.in);
         * 
         * id_al = sc.nextInt(); try {
         * 
         * con = DriverManager.getConnection ( connUrl, us, pass ); // La
         * conexión en sí, pasandole la url, usuario y password.
         * PreparedStatement stmt =
         * con.prepareStatement("SELECT * FROM alumno WHERE id_alumno = " +
         * id_al); ResultSet rs = stmt.executeQuery();
         * 
         * System.out.println( "Alumno\n" ); while ( rs.next() ) {
         * System.out.println( rs.getString("id_alumno") + " " +
         * rs.getString("nombre") ); } if ( con != null ) System.out.println(
         * "Todo correcto" );
         * 
         * rs.close(); stmt.close(); con.close();
         * 
         * } catch ( SQLException e ) { System.out.print( "Fallo\n" );
         * e.printStackTrace(); }
         */
    }

}