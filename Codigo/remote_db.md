# Base de datos Remota

Se han hecho pruebas en los siguientes servicios:

- **freewebhostingarea.com**

> No dejan importar scripts .sql, tampoco funciona conectarse por phpMyAdmin, ya que
> lo tienen configurado con acceso desde localhost, y no facilitan la ip de la bbdd para
> conectarse desde consola.

- **remotemysql.com**

> No te permiten crear una bbdd, o hacer login desde un cliente de mysql.


- **freehostia.com**

> Están enfocados en webs no en mysql, a la hora del registro te piden un dominio valido y
> tarjeta de credito/debito.

- **Google Cloud SQL**

> Te dan un periodo de prueba durante 1 mes pero a la hora de registrarse tienes que
> introducir la cuenta del banco, he intentado probar con generación de tarjetas de
> credito falsas y no funciona.

- **awardspace.net**

> Te permite crear una bbdd, también te permite configurarla mediante phpMyAdmin y tiene
> algunas configuraciones mas, pero no te permite la conexion remota a la bbdd con un
> cliente de mysql.
