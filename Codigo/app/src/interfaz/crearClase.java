package interfaz;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import conexiones.Conexiones;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Cursor;

public class crearClase extends JFrame {

    private JPanel       contentPane;
    private final JLabel lblRandomapp = new JLabel ( "Añadir Clase" );
    private JTextField   textField;

    String urlConexion = "jdbc:mysql://localhost:3306/scrum?serverTimezone=UTC";
    String usuario     = "admin";
    String password    = "admin123";

    public crearClase () {
        JFrame frame = new JFrame ();
        frame.setDefaultCloseOperation ( HIDE_ON_CLOSE );
        setBounds ( 100, 100, 571, 490 );
        frame.setTitle ( "Random App -  Añadir Clase" );
        frame.setResizable ( false );
        contentPane = new JPanel ();
        setContentPane ( contentPane );
        contentPane.setLayout ( null );

        // Primer Panel (Rojo)
        JPanel panel = new JPanel ();
        panel.setBackground ( new Color ( 255, 0, 0 ) );
        panel.setBounds ( 0, 0, 571, 116 );
        contentPane.add ( panel );
        panel.setLayout ( null );
        lblRandomapp.setBounds ( 0, 12, 571, 116 );
        lblRandomapp.setFont ( new Font ( "SansSerif", Font.BOLD, 49 ) );
        lblRandomapp.setHorizontalAlignment ( SwingConstants.CENTER );
        lblRandomapp.setForeground ( Color.WHITE );
        panel.add ( lblRandomapp );

        // Segundo Panel el blanco que contiene botones
        JPanel panel_1 = new JPanel ();
        panel_1.setBackground ( new Color ( 255, 255, 255 ) );
        panel_1.setBounds ( 0, 118, 571, 341 );
        contentPane.add ( panel_1 );
        panel_1.setLayout ( null );

        // Input 'nombre de la clase'
        JLabel lblNombreDeLa = new JLabel ( "Nombre de la Clase" );
        lblNombreDeLa.setFont ( new Font ( "SansSerif", Font.BOLD, 14 ) );
        lblNombreDeLa.setBounds ( 190, 28, 168, 27 );
        panel_1.add ( lblNombreDeLa );

        textField = new JTextField ();
        textField.setBounds ( 149, 67, 248, 32 );
        panel_1.add ( textField );
        textField.setColumns ( 10 );

        // Boton Añadir Clase
        JButton botonCrear = new JButton ( "Añadir Clase" );
        botonCrear.setCursor ( Cursor.getPredefinedCursor ( Cursor.HAND_CURSOR ) );
        botonCrear.setForeground ( new Color ( 255, 255, 255 ) );
        botonCrear.setBackground ( new Color ( 255, 0, 0 ) );
        botonCrear.setBounds ( 206, 249, 140, 40 );
        botonCrear.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( botonCrear );

        // Crea una nueva clase en la BBDD y cierra la ventana
        botonCrear.addActionListener ( new ActionListener () {
            public void actionPerformed ( ActionEvent e ) {
                String     nombreClase = textField.getText ();
                Connection conexion    = Conexiones.conectarDB ( urlConexion, usuario, password );
                Conexiones.crearClase ( conexion, nombreClase );
                dispose ();

            }
        } );
        
         //Boton Atras
        JButton botonAtras = new JButton("Atrás");
        botonAtras.setForeground(Color.WHITE);
        botonAtras.setBounds(450, 304, 92, 25);
        botonAtras.setBackground ( new Color ( 255, 0, 0 ) );
        panel_1.add(botonAtras);
        //Vuelve a la clase Inicio
        botonAtras.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        	Inicio insertarInicio = new Inicio ();
        	insertarInicio.setVisible ( true );
            insertarInicio.setLocationRelativeTo ( null );
            dispose();
        	}
        });
		

    }
}
