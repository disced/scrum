package interfaz;

import javax.swing.*;
import java.awt.event.*;

public class Random extends JFrame implements ItemListener {
    private JComboBox Ventana_Random;

    public Random () {
        setLayout ( null );
        Ventana_Random = new JComboBox ();
        Ventana_Random.setBounds ( 35, 10, 330, 30 );
        add ( Ventana_Random );

        Ventana_Random.addItem ( "1º DAM" );
        Ventana_Random.addItem ( "1º Telecomunicaciones" );
        Ventana_Random.addItem ( "2º DAM" );
        Ventana_Random.addItemListener ( this );

    }

    public void itemStateChanged ( ItemEvent evento_capturado ) {

        if ( evento_capturado.getSource () == Ventana_Random ) {

            String Clase_elegida = Ventana_Random.getSelectedItem ().toString ();

            setTitle ( Clase_elegida );

        }

    }

    public void Boton_Random () {
        JButton Boton_Random;
        Boton_Random = new JButton ( "Random" );
        setLayout ( null );
        Boton_Random.setBounds ( 35, 30, 100, 30 );
        add ( Boton_Random );
        Boton_Random.addActionListener ( (ActionListener)this );

    }

    public void actionPerformed ( ActionEvent Random_Event ) {

        if ( Random_Event.getSource () == Boton_Random ) {

        }

    }

    public static void main ( String[] args ) {

        // TODO Auto-generated method stub

        Random Ventana_Random = new Random ();
        Ventana_Random.setBounds ( 0, 0, 400, 400 );
        Ventana_Random.setVisible ( true );
        Ventana_Random.setResizable ( false );
        Ventana_Random.setLocationRelativeTo ( null );
        Ventana_Random.setTitle ( "Seleccione Clase" );

    }

}
