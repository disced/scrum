package interfaz;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Inicio extends JFrame {

    private JPanel       contentPane;
    private final JLabel lblRandomapp = new JLabel ( "RandomAPP" );

    public Inicio () {
        setDefaultCloseOperation ( JFrame.EXIT_ON_CLOSE );
        setBounds ( 100, 100, 571, 490 );
        setTitle ( "Random App" );
        setResizable ( false );
        contentPane = new JPanel ();
        setContentPane ( contentPane );
        contentPane.setLayout ( null );

        // Primer Panel (Rojo)
        JPanel panel = new JPanel ();
        panel.setBackground ( new Color ( 255, 0, 0 ) );
        panel.setBounds ( 0, 0, 571, 116 );
        contentPane.add ( panel );
        panel.setLayout ( null );
        lblRandomapp.setBounds ( 98, 12, 369, 99 );
        lblRandomapp.setFont ( new Font ( "Sans Serif", Font.BOLD, 49 ) );
        lblRandomapp.setHorizontalAlignment ( SwingConstants.CENTER );
        lblRandomapp.setForeground ( Color.WHITE );
        panel.add ( lblRandomapp );

        // Segundo Panel el blanco que contiene botones
        JPanel panel_1 = new JPanel ();
        panel_1.setBackground ( new Color ( 255, 255, 255 ) );
        panel_1.setBounds ( 0, 119, 571, 341 );
        contentPane.add ( panel_1 );
        panel_1.setLayout ( null );

        // Boton Random
        JButton botonRandom = new JButton ( "Random" );
        botonRandom.setForeground ( Color.WHITE );
        botonRandom.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        botonRandom.setBackground ( new Color ( 255, 0, 0 ) );
        botonRandom.setBounds ( 204, 78, 165, 40 );
        panel_1.add ( botonRandom );

        // Abre la ventana aleatorio
        botonRandom.addActionListener ( new ActionListener () {
            public void actionPerformed ( ActionEvent e ) {
                aleatorio generarAleatorio = new aleatorio ();
                generarAleatorio.setVisible ( true );
                generarAleatorio.setLocationRelativeTo ( null );
                dispose();
            }
        } );

        // Boton Añadir Clase
        JButton botonClases = new JButton ( "Añadir Clase" );
        botonClases.setForeground ( new Color ( 255, 255, 255 ) );
        botonClases.setBounds ( 204, 130, 165, 40 );
        botonClases.setBackground ( new Color ( 255, 0, 0 ) );
        botonClases.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( botonClases );

        // Abre la ventana crearClase
        botonClases.addActionListener ( new ActionListener () {
            public void actionPerformed ( ActionEvent e ) {
                crearClase insertarClase = new crearClase ();
                insertarClase.setVisible ( true );
                insertarClase.setLocationRelativeTo ( null );
                dispose();
            }
        } );

        // Boton Alumnos
        JButton botonAlumnos = new JButton ( "Añadir Alumno\n" );
        botonAlumnos.setForeground ( Color.WHITE );
        botonAlumnos.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        botonAlumnos.setBackground ( new Color ( 255, 0, 0 ) );
        botonAlumnos.setBounds ( 204, 182, 165, 40 );
        panel_1.add ( botonAlumnos );

        // Abre la ventana crearAlumno
        botonAlumnos.addActionListener ( new ActionListener () {
            public void actionPerformed ( ActionEvent e ) {
                crearAlumno insertarAlumno = new crearAlumno ();
                insertarAlumno.setVisible ( true );
                insertarAlumno.setLocationRelativeTo ( null );
                dispose();
            }
        } );

        // Boton Estadistica
        JButton botonEstadisitca = new JButton ( "Estadística" );
        botonEstadisitca.setForeground ( new Color ( 255, 255, 255 ) );
        botonEstadisitca.setBackground ( new Color ( 255, 0, 0 ) );
        botonEstadisitca.setBounds ( 204, 234, 165, 40 );
        botonEstadisitca.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( botonEstadisitca );

        // Abre la ventana estadistica
        botonEstadisitca.addActionListener ( new ActionListener () {
            public void actionPerformed ( ActionEvent e ) {
                estadistica est = new estadistica ();
                est.setVisible ( true );
                est.setLocationRelativeTo ( null );
                dispose();
            }
        } );

    }
}
