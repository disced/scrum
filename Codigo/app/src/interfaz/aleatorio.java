package interfaz;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.stream.IntStream;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Cursor;
import javax.swing.JComboBox;
import conexiones.*;
import logica.*;

public class aleatorio extends JFrame {
    String urlConexion = "jdbc:mysql://localhost:3306/scrum?serverTimezone=UTC";
    String usuario     = "admin";
    String password    = "admin123";

    Connection con = Conexiones.conectarDB ( urlConexion, usuario, password );

    ResultSet resultados;

    private JPanel       contentPane;
    private final JLabel lblRandomapp = new JLabel ( "Alumno Aleatorio" );
    private JTextField   apellido1;
    int                  tot;

    public aleatorio () {

        setDefaultCloseOperation ( JFrame.HIDE_ON_CLOSE );
        setBounds ( 100, 100, 571, 490 );
        setTitle ( "Random App - Aleatorio" );
        setResizable ( false );
        contentPane = new JPanel ();
        setContentPane ( contentPane );
        contentPane.setLayout ( null );

        // Primer Panel (Rojo)
        JPanel panel = new JPanel ();
        panel.setBackground ( new Color ( 255, 0, 0 ) );
        panel.setBounds ( 0, 0, 571, 116 );
        contentPane.add ( panel );
        panel.setLayout ( null );
        lblRandomapp.setBounds ( 0, 12, 571, 116 );
        lblRandomapp.setFont ( new Font ( "SansSerif", Font.BOLD, 49 ) );
        lblRandomapp.setHorizontalAlignment ( SwingConstants.CENTER );
        lblRandomapp.setForeground ( Color.WHITE );
        panel.add ( lblRandomapp );

        // Segundo Panel el blanco que contiene botones
        JPanel panel_1 = new JPanel ();
        panel_1.setToolTipText ( "" );
        panel_1.setBackground ( new Color ( 255, 255, 255 ) );
        panel_1.setBounds ( 0, 118, 571, 341 );
        contentPane.add ( panel_1 );
        panel_1.setLayout ( null );

        // Label e Input nombre
        JLabel lblNombre = new JLabel ( "Nombre" );
        lblNombre.setBounds ( 134, 164, 70, 15 );
        lblNombre.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( lblNombre );

        JTextField nombre = new JTextField ( 35 );
        nombre.setOpaque ( false );
        nombre.setEditable ( false );
        nombre.setEnabled ( false );
        nombre.setSelectedTextColor ( Color.BLACK );
        nombre.setDisabledTextColor ( Color.DARK_GRAY );
        nombre.setForeground ( Color.DARK_GRAY );
        nombre.setVerifyInputWhenFocusTarget ( false );
        nombre.setBounds ( 208, 156, 182, 32 );
        nombre.setFont ( new Font ( "SansSerif", Font.BOLD, 14 ) );
        panel_1.add ( nombre );

        // Label e Input apellido1
        JLabel lblApellido1 = new JLabel ( "Apellido" );
        lblApellido1.setBounds ( 134, 216, 130, 15 );
        lblApellido1.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( lblApellido1 );

        JTextField apellido1 = new JTextField ( 35 );
        apellido1.setVerifyInputWhenFocusTarget ( false );
        apellido1.setDisabledTextColor ( Color.DARK_GRAY );
        apellido1.setBackground ( Color.WHITE );
        apellido1.setCaretColor ( Color.BLACK );
        apellido1.setForeground ( Color.DARK_GRAY );
        apellido1.setEnabled ( false );
        apellido1.setEditable ( false );
        apellido1.setDragEnabled ( true );
        apellido1.setBounds ( 208, 208, 182, 32 );
        apellido1.setFont ( new Font ( "SansSerif", Font.BOLD, 14 ) );
        panel_1.add ( apellido1 );

        // Campo Texto 'Seleciona Clase'
        JLabel lblSeleccionaClase = new JLabel ( "Selecciona Clase" );
        lblSeleccionaClase.setFont ( new Font ( "SansSerif", Font.BOLD, 14 ) );
        lblSeleccionaClase.setBounds ( 205, 12, 133, 28 );
        panel_1.add ( lblSeleccionaClase );

        // Seleccionable de la clase
        JComboBox comboBox = new JComboBox ();
        comboBox.setCursor ( Cursor.getPredefinedCursor ( Cursor.HAND_CURSOR ) );
        comboBox.setBackground ( Color.WHITE );
        comboBox.setBounds ( 134, 40, 285, 32 );
        panel_1.add ( comboBox );
        ResultSet res = Conexiones.consultaClaseAlumno ( con );
        try {
            while ( res.next () ) {
                comboBox.addItem ( res.getString ( "nombre_clase" ) );
            }
        } catch ( SQLException e ) {
            e.printStackTrace ();
        }

        // Boton Random
        JButton botonAleatorio = new JButton ( "Generar Aleatorio" );
        botonAleatorio.setCursor ( Cursor.getPredefinedCursor ( Cursor.HAND_CURSOR ) );
        botonAleatorio.setForeground ( new Color ( 255, 255, 255 ) );
        botonAleatorio.setBackground ( new Color ( 255, 0, 0 ) );
        botonAleatorio.setBounds ( 178, 84, 192, 40 );
        botonAleatorio.setFont ( new Font ( "SansSerif", Font.BOLD, 14 ) );
        panel_1.add ( botonAleatorio );

        botonAleatorio.addActionListener ( new ActionListener () {
            public void actionPerformed ( ActionEvent e ) {
                int       total;
                String    claseElegida = (String)comboBox.getSelectedItem ();
                ResultSet totalAlumnos = Conexiones.consultaTotalAlumno ( con, claseElegida );
                try {
                    totalAlumnos.next ();
                    total        = totalAlumnos.getInt ( 1 );
                    totalAlumnos = Conexiones.consultaAlumno ( con, claseElegida );
                    if ( total != 0 ) {
                        int[] listaAlumnos = IntStream.range ( 0, total ).toArray ();
                        int   i            = 0;
                        while ( totalAlumnos.next () ) {
                            listaAlumnos[i] = totalAlumnos.getInt ( "id_alumno" );
                            i++;
                        }
                        total = Rand.generaRand ( total );
                        System.out.println ( total );
                        System.out.println ( listaAlumnos[total] );
                        totalAlumnos = Conexiones.consultaAlumnoId ( con, listaAlumnos[total] );
                        totalAlumnos.next ();
                        nombre.setText ( totalAlumnos.getString ( "nombre" ) );
                        apellido1.setText ( totalAlumnos.getString ( "apellido1" ) );
                        tot = listaAlumnos[total];
                    } else {
                        nombre.setText ( "Clase Vacia" );
                        apellido1.setText ( "" );
                    }

                } catch ( SQLException e2 ) {
                    e2.printStackTrace ();
                }
            }
        } );

        // Boton Positivo
        JButton botonPositivo = new JButton ( "Positivo" );
        botonPositivo.setCursor ( Cursor.getPredefinedCursor ( Cursor.HAND_CURSOR ) );
        botonPositivo.setForeground ( Color.WHITE );
        botonPositivo.setBackground ( new Color ( 0, 153, 51 ) );
        botonPositivo.setBounds ( 12, 289, 99, 40 );
        panel_1.add ( botonPositivo );
        botonPositivo.addActionListener ( new ActionListener () {
            public void actionPerformed ( ActionEvent e ) {
                Conexiones.puntoPositivo ( con, tot );
                nombre.setText ( "" );
                apellido1.setText ( "" );
                tot = -1;

                //dispose();
            }
        } );

        // Boton Negativo
        JButton botonNegativo = new JButton ( "Negativo" );
        botonNegativo.setForeground ( Color.WHITE );
        botonNegativo.setBackground ( new Color ( 204, 0, 51 ) );
        botonNegativo.setBounds ( 460, 289, 99, 40 );
        panel_1.add ( botonNegativo );
        botonNegativo.addActionListener ( new ActionListener () {
            public void actionPerformed ( ActionEvent e ) {
                Conexiones.puntoNegativo ( con, tot );
                nombre.setText ( "" );
                apellido1.setText ( "" );
                tot = -1;
                //dispose ();
            }
        } );
        
         // Boton Atras
        JButton botonAtras = new JButton ( "Atrás" );
        botonAtras.setBounds(231, 297, 117, 25);
        botonAtras.setBackground ( new Color ( 255, 0, 0 ) );
        botonAtras.setForeground ( Color.WHITE );
        panel_1.add(botonAtras);
        botonAtras.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        	Inicio insertarInicio = new Inicio ();
            insertarInicio.setVisible ( true );
            insertarInicio.setLocationRelativeTo ( null );
            dispose();
        	}
        });
		

    }
}
