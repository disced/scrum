package interfaz;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import conexiones.Conexiones;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Cursor;
import javax.swing.JComboBox;

public class estadistica extends JFrame {

    private JPanel contentPane;
    private final JLabel lblRandomapp = new JLabel ( "Estadistica" );

    String urlConexion = "jdbc:mysql://localhost:3306/scrum?serverTimezone=UTC";
    String usuario = "admin";
    String password = "admin123";

    public estadistica () {
        JFrame frame = new JFrame ();
        frame.setDefaultCloseOperation ( HIDE_ON_CLOSE );
        setBounds ( 100, 100, 571, 490 );
        frame.setTitle ( "Random App -  Añadir Clase" );
        frame.setResizable ( false );
        contentPane = new JPanel ();
        setContentPane ( contentPane );
        contentPane.setLayout ( null );

        // Primer Panel (Rojo)
        JPanel panel = new JPanel ();
        panel.setBackground ( new Color ( 255, 0, 0 ) );
        panel.setBounds ( 0, 0, 571, 116 );
        contentPane.add ( panel );
        panel.setLayout ( null );
        lblRandomapp.setBounds ( 0, 12, 571, 116 );
        lblRandomapp.setFont ( new Font ( "SansSerif", Font.BOLD, 49 ) );
        lblRandomapp.setHorizontalAlignment ( SwingConstants.CENTER );
        lblRandomapp.setForeground ( Color.WHITE );
        panel.add ( lblRandomapp );

        // Segundo Panel el blanco que contiene botones
        JPanel panel_1 = new JPanel ();
        panel_1.setBackground ( new Color ( 255, 255, 255 ) );
        panel_1.setBounds ( 0, 118, 571, 341 );
        contentPane.add ( panel_1 );
        panel_1.setLayout ( null );
        
        JComboBox claseBox = new JComboBox();
        claseBox.setBackground(Color.WHITE);
        claseBox.setBounds(23, 29, 188, 32);
        panel_1.add(claseBox);
        
        JComboBox alumnoBox = new JComboBox();
        alumnoBox.setBackground(Color.WHITE);
        alumnoBox.setBounds(23, 98, 188, 32);
        panel_1.add(alumnoBox);
        
        JButton botonSeleccionar = new JButton("Mostrar Datos");
        botonSeleccionar.setForeground(Color.WHITE);
        botonSeleccionar.setFont(new Font("Dialog", Font.BOLD, 14));
        botonSeleccionar.setBackground(Color.RED);
        botonSeleccionar.setBounds(23, 142, 188, 40);
        panel_1.add(botonSeleccionar);
        
        JLabel lblSeleccionaClase = new JLabel("Selecciona Clase");
        lblSeleccionaClase.setBounds(23, 12, 140, 15);
        panel_1.add(lblSeleccionaClase);
        
        JLabel lblSeleccionaAlumno = new JLabel("Selecciona Alumno");
        lblSeleccionaAlumno.setBounds(23, 76, 140, 15);
        panel_1.add(lblSeleccionaAlumno);
    
          //Boton Atras
        JButton botonAtras = new JButton("Atrás");
        botonAtras.setForeground(Color.WHITE);
        botonAtras.setBounds(450, 304, 92, 25);
        botonAtras.setBackground ( new Color ( 255, 0, 0 ) );
        panel_1.add(botonAtras);
        //Vuelve a la clase Inicio
        botonAtras.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        	Inicio insertarInicio = new Inicio ();
        	insertarInicio.setVisible ( true );
            insertarInicio.setLocationRelativeTo ( null );
            dispose();
        	}
        });
        
    }
}
