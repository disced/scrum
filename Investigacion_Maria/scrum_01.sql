DROP DATABASE IF EXISTS scrum_01;
CREATE DATABASE scrum_01;
USE scrum_01;

CREATE TABLE alumno (
	id_alumno INT AUTO_INCREMENT,
	nombre VARCHAR(80),
	apellido1 VARCHAR(60),
	apellido2 VARCHAR(60),

	CONSTRAINT id_alumno_PK PRIMARY KEY (id_alumno) 
);

CREATE TABLE puntuacion (
	id_alumno INT,
	id_n_lista SMALLINT,
	n_correctas SMALLINT,
	porcentaje_acierto INT,
	puntuacion INT,
	CONSTRAINT puntuacion_PK PRIMARY KEY (id_alumno, id_n_lista),
	CONSTRAINT puntuacion_alumno_FK FOREIGN KEY (id_alumno) REFERENCES alumno	(id_alumno)
);
