package conn_bbdd; 
import java.sql.*; 
import java.util.logging.Logger;
import java.util.Scanner;
import java.util.logging.Level;

public class conn {

    public static void main(String[] args) {
    	// Datos de la conexión, en mi maquina
        String connUrl = "jdbc:mysql://localhost:3306/scrum_01?serverTimezone=UTC",
               us      = "admin", 			// Modificar el usuario y password segun el usuario de cada uno en mysql.
               pass    = "admin@123";

        Connection con  = null;				// Variable de conexion.
        int id_al;
        Scanner sc = new Scanner (System.in);
        
        id_al = sc.nextInt();
        try {
        	con = DriverManager.getConnection ( connUrl, us, pass ); 	// La conexión en sí, pasandole la url, usuario y password.

        	PreparedStatement stmt = con.prepareStatement("SELECT * FROM alumno WHERE id_alumno = " + id_al);
        	ResultSet rs = stmt.executeQuery();

        	System.out.println( "Alumno\n" );
        	while ( rs.next() ) {
        	    System.out.println( rs.getString("id_alumno") + " " + rs.getString("nombre") );
        	}
        	if ( con != null )
        		System.out.println( "Todo correcto" );
        	
        	rs.close();
        	stmt.close();
        	

        } catch ( SQLException e ) {
        	System.out.print( "Fallo\n" );
        	e.printStackTrace();
        }
    }
}
