CREATE DATABASE scrum_01;
USE scrum_01;

CREATE TABLE alumno (
    id_alumno INT AUTO_INCREMENT,
    nombre VARCHAR(80),
    apellido1 VARCHAR(60),
    apellido2 VARCHAR(60),
    CONSTRAINT alumno_pk PRIMARY KEY (id_alumno)
);

CREATE TABLE puntuacion (
    id_alumno INT,
    id_n_lista SMALLINT,
    n_correctas SMALLINT,
    n_incorrectas SMALLINT,
    porcentaje_acierto INT,
    puntuacion INT,
    CONSTRAINT puntuacion_pk PRIMARY KEY (id_alumno, id_n_lista),
    CONSTRAINT puntuacion_alumno_fk FOREIGN KEY ( id_alumno ) REFERENCES alumno ( id_alumno )
);
