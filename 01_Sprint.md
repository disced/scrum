# Reunion Sprint Planing

## Sprint Backlog

### Base de Datos
---

#### Tablas

1. **Alumno**

  - idAlumno
  - nombre
  - apellido1
  - apellido2

2. **Puntuacion**

  - idAlumno
  - afirmativas
  - negativas

### Conexión a BBDD

java.sql

Programa que se conecte a la base de datos y consiga hacer consultas, eliminar y modificar datos.

