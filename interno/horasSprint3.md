# Horas totales Dailys Scrum

## Horas Por Dia

| Dias | Dragos | Adrian | Maria | Miguel |
| ---- | ------ | -----  | ----- | -----  |
| 04/05/2020 | 2 | 2,5   | 1     | 2,5    |
| 06/05/2020 | 1,5 | 1,5 | 1     | 3      |
| 08/05/2020 | 2 | 2 | 1     | 2      |


## Total Horas
| Dragos | Adrian | Maria | Miguel |
| -----  | -----  | ----- | -----  |
|  5,5   |  6     |  3    |   7,5  |

