# DAILY SCRUM

Reunión diaria del equipo.
---
## Día 15/04/2020

### ¿Que hice?

> - Dragos: Aprender a utilizar MySQL Wokrbench y el script de implementación de la BBDD para sacar el modelo ER con MySQL WorkBench
> - Adrian: Mirar cómo hacer script Números Aleatorios Java.
         Instalar Eclipse Ubuntu.
> - Maria: Modelo entidad relación de la base de datos del proyecto, y la instalación del jdk y eclipse en ubuntu.
> - Miguel: Modelo entidad relación de la base de datos del proyecto y modelo optimizado.

### ¿Que voy a hacer?
> - Dragos: Mejorar el script de la base de datos e introducir datos de prueba, para hacer consultas enfocadas a futuro, añadir puntos, cálculos, etc.
> - Adrian: Consultas Base de Datos
> - Maria: Instalación de paquetes de java y terminar la base de datos.
> - Miguel: Instalación del jdk  y jre versión 11 y eclipse en ubuntu.

### ¿Que problemas he tenido?
> - Dragos: Con MySQL Workbench a la hora de conectarme, he creado un nuevo usuario en mysql y le he dado todos los permisos.
> - Adrian: Necesito el listado de alumnos.
> - No me deja instalar sudo apt install openjdk-11-jre-headless
> - Maria: No me ha dejado instalar eclipse directamente en la terminal, y he tenido que  descargarla en la página oficial.
> - Miguel: He tenido problemas con las versiones de ubuntu y he tenido que cambiar de versión de la 19.0 a la 18.06.
---

## 16/04/2020

### ¿Que hice?
> - Dragos: Insertar alumnos y archivo ‘para_java’
> - Adrian: Consultas a la base de datos. Mirar cómo conectar la base de datos con Java
> - Maria: Añadir datos a las tablas, hacer consultas.
> - Miguel: Instalación del jdk  y jre versión 11 y eclipse en ubuntu.

### ¿Que voy a hacer?
> - Dragos: Aprender a conectarme a una bbdd con java.
> - Adrian: Conectar la base de datos, hacer consultas y modificaciones en la base.
> - Maria: Mirar cómo conectar una base de datos con Java, y las función para poner el número aleatorio.
> - Miguel: Instalación del plugin grafico para Eclipse y empezar a documentarme para hacer interfaz.

### ¿Que problemas he tenido?
> - Dragos: ha fallado la conexion desde java a mysql
> - Adrian:
> - Maria: He tenido problemas en comprender la conexión de mysql con java.
> - Miguel: No ha habido ningún problema.
---

## 17/04/2020

### ¿Que hice?
> - Dragos: He seguido con la conexión desde java a mysql y lo he conseguido
> - Adrian:
> - Maria: mirar la conexión entre mysql y java.
> - Miguel:

### ¿Que voy a hacer?
> - Dragos:
> - Adrian:
> - Maria: Intentar hacer la conexión con un pequeño ejemplo.
> - Miguel:

### ¿Que problemas he tenido?
> - Dragos: Falla la conexion por que hay que descargarse el driver de mysql y luego referenciar el archivo mysql-connector.jar en nuestro proyecto https://www.ecodeup.com/como-conectarse-a-mysql-desde-java-utilizando-jdbc-driver-desde-cero/. Al tener el driver tambien fallaba por el timezone y en la url de conexion he utilizado el UTC y ha sido satisfactorio.
> - Adrian:
> - Maria:
> - Miguel:
---

## Día 20/04/2020

### ¿Que hice?
> - Dragos: He leido varias cosas en la API de JAVA sobre java.sql y he hecho consultas básicas.
> - Adrian: Consultar como hacer Sprint Review.
> - Maria: Consultar como hacer Sprint Review.
> - Miguel: Consultar como hacer interfaces sencillas con Windowns builder.

### ¿Que voy a hacer?
> - Dragos: Crear en src una clase de conexion una clase de consultas.
> - Adrian: Informarme y prepararme sobre el Sprint Demo.
> - Maria: Preparar de como realizar y estructurar el Sprint Reviw.
> - Miguel: Poner en práctica interfaces sencillas.

### ¿Que problemas he tenido?
> - Dragos: Organizacion de las clases, el codigo, sin nigún problema técnico.
        Como funcionan las clases, los metodos, los objetos con Java.
> - Adrian: Ningún problema.
> - Maria: Ningún problema.
> - Miguel: Ningún problema.
---

## Día 21/04/2020

### ¿Que hice?
> - Dragos: Consulta especificando un id con usuario.
> - Adrian: Crear el Sprint 1 Review.
> - Maria: Crear el Sprint 1 Review.
> - Miguel: Consultar sobre la interfaz gráfica, creación de botones sencillos con Java.

### ¿Que voy a hacer?
> - Dragos: Crear en src una clase de conexion una clase de consultas.
> - Adrian: Sprint Review.
> - Maria: Sprint Review.
> - Miguel: Intentar crear interfaces con botones con Java.

### ¿Que problemas he tenido?
> - Dragos: Organización de las clases, el código, sin nigun problema técnico. Como funcionan las clases, los metodos, los objetos con Java.
> - Adrian: Ningún problema.
> - Maria: Ningún problema.
> - Miguel: Ningún problema.
---

## Día 04/05/2020


### ¿Que hice?
> - Dragos: Una clase separada con las consultas.
> - Adrian: He probado java.swing con el plugin Window Maker en eclipse, he creado ventanas.
> - Maria:  He hecho el ER de la nueva tabla.
> - Miguel: He estudiado mas a fondo como funciona java.swing, he probado a hacer una ventana con boton, tambien he mirado como funcionan los eventos con los botones.

### ¿Que voy a hacer?
> - Dragos: Seguir con todas las consultas
> - Adrian: Configurar, poner colores (temporales), y hacer la interfaz principal.
> - Maria: Crear el archivo .sql e investigar sobre servidor de mysql remoto.
> - Miguel: Ventana de seleccion de clase y generar el aleatorio.

### ¿Que problemas he tenido?
> - Dragos: Problemas con eclipse, en el workspace y problemas al importar el proyecto.
> - Adrian: No entendia muy bien como funciona del todo.
> - Maria:  Nignuno.
> - Miguel: Ninguno de momento.

### Horas

> - Dragos : 2 H
> - Adrian : 2,5 H
> - Maria : 1 H
> - Miguel : 2.5 H
---


## Día 06/05/2020


### ¿Que hice?
> - Dragos: Metodos de aumentar/disminuir puntos, consultar total de las clases y consultar alumnos de una clase.
> - Adrian: Buscar información sobre como abrir nuevas ventanas mediante botonoes y cambiar el diseño de los botones.
> - Maria:  He hecho la nueva BBDD.
> - Miguel: he visto videos de un curso de Java mas detallado que explica todo el codigo y he programado una ventada con un desplegable y un evento que cambia el titulo de la pagina.

### ¿Que voy a hacer?
> - Dragos: Seguir con los metodos de consultas, hacer dos metodos nuevos, crear un alumno y crear una clase.
> - Adrian: Terminar la interfaz principal y añadir nuevas ventas con botones.
> - Maria:  Buscar un servidor remoto gratuito de MySQL para poder ejecutar la aplicacion en cualquier ordenador.
> - Miguel: La ventana de Random.

### ¿Que problemas he tenido?
> - Dragos: Al no tener la BBDD lista del todo es complicado hacer la programación sin ver el resultado de la bbdd.
> - Adrian: A la hora de configurar el diseño de nuevos botones, me da error, no se como configurarlo y al maximizar la ventada no cambia el interior de tamaño.
> - Maria: estoy investigando sobre como hacer la base de datos remota porque no he entendido mucho como poder hacerla del todo bien.
> - Miguel: Estoy empleando mas tiempo del acordado en el planing poker.

### Horas

> - Dragos : 1,5 H
> - Adrian : 1,5 H
> - Maria :  1 H
> - Miguel : 3 H
---

## Día 08/05/2020


### ¿Que hice?
> - Dragos: Modificar el proyecto para ponerlo en un unico proyecto con diferentes paquetes y clases, mayor especificacion en tareas de Trello, apuntar las horas de PlaningPoker en Trello.
> - Adrian: Cambiar el diseño de la interfaz principal, descargar el Driver de MySQL para vincularlo al proyecto en Eclipse.
> - Maria: Investigar un host de Base de datos remota.
> - Miguel: He seguido con los videos de Java.

### ¿Que voy a hacer?
> - Dragos: Me pondré con el tema de la interfaz grafica junto a Adri y Miguel.
> - Adrian: Interfaz de añadir alumno.
> - Maria: Seguir investigando sobre la bbdd remota y adjuntar un archivo con los datos.
> - Miguel: Terminar la ventana de generar un alumno aleatorio.

### ¿Que problemas he tenido?
> - Dragos: Ninguno
> - Adrian: Ninguno
> - Maria:  En uno de ellos no he entendido muy bien como funciona el crear la base de datos.
> - Miguel: Demasiada informacion para aprender en muy poco tiempo.

### Horas

> - Dragos : 2 H
> - Adrian : 2 H
> - Maria :  1 H
> - Miguel : 2 H
---


## Día 11/05/2020


### ¿Que hice?
> - Dragos: Ventanas insertar alumno, insertar clase, ventana random  modificacion en el tamño del campo nombre clase en la BBDD, permite crear clases y alumnos.
> - Adrian: Investigar sobre la generación de graficas 3D y reportes estadisticos con Java.
> - Maria:  Seguir con el tema de la base de datos remota.
> - Miguel: Preparar la demo del Sprint Review.

### ¿Que voy a hacer?
> - Dragos: Finalizar la funcionalidad de las ventanas.
> - Adrian: Comenzar a hacer la ventana de estadistica.
> - Maria:  Seguir buscando algun host de mysql remoto.
> - Miguel: Ponerme con Dragos a terminar el tema de consultar alumnos segun las clases.

### ¿Que problemas he tenido?
> - Dragos: Ninguno.
> - Adrian: Ninguno.
> - Maria:  No se muy bien como funcionan estos tipos de hosting y como se crean las bbdd en ellos
> - Miguel: Ninguno.

### Horas

> - Dragos :  4 H
> - Adrian :  2 H
> - Maria :   2 H
> - Miguel :  2 H
---

## Día 13/05/2020

### ¿Que hice?
>- Dragos: El seleccionable de las clases en el random ,que funcione el random del alumno  con la función que hizo Adrian, sleccionable de la clase en  generar alumno.
>- Adrian: Intentar solucionar el porque al hacer un pull del proyecto no me deja ejecutarlo luego en eclipse. Pensar sobre el diseño de la App: hacer una página sencilla donde poder descargar la App.
>- Miguel: Investigar sobre como hacer un ejecutable en java, para que cualquier persona pueda descargar el software.
>- Maria:  Seguir buscando algún host de mysql remoto.

### ¿Que voy hacer?
>- Dragos: Que funcionen los botones de punto positivo y negativo.
>- Adrian: Intentar solucionar el problema y ponerme con la interfaz gráfica.
>- Miguel: Seguir investigando.
>- Maria:  Intentar hacer en mi ordenador la base de datos remota con toda la información recolectada previamente de vídesos,páginas,etc.

### ¿Qué problemas he tenido?
>- Dragos:  Ninguno, ya esta siendo más fácil crear las cosas.
>- Adrian: No he conseguido ejecutar la App desde eclipse.
>- Miguel: Aún no he conseguido hacerlo.
>- Maria:  Aún no he conseguido hacerlo.

### Horas
> - Dragos :  4H
> - Adrian :  2H
> - Maria :   1H
> - Miguel :  1H
---

## Día 15/05/2020

### ¿Que hice?
>- Dragos: Conseguir guardar puntos positivos y negativos de un alumno en concreto en la tabla puntuación.
>- Adrian: Solucionar el problema a la hora de ejecutar el proyecto desde eclipse.
>- Miguel: Buscar sitios para hacer webs y alojamiento gratis.
>- Maria:  Intentar hacer en mi ordenador la base de datos remota con toda la información recolectada previamente de vídesos,páginas,etc.

### ¿Que voy hacer?
>- Dragos: Cambiar que no se cierre la ventana de random cuando  guarde algún punto, que se cambie de usuario a vacio.
>- Adrian: Buscar información para ejecutar la aplicación desde cualquier Sistema Operativo Windows, Linux...
>- Miguel: Empezar a hacer la página web.
>- Maria:  En la terminal me instalé el ssh en mi ordenador para poder hacer la conexión remota de la base de datos.


### ¿Qué problemas he tenido?
>- Dragos: Ninguno.
>- Adrian: Ninguno.
>- Miguel: Falta de sitios gratuitos.
>- Maria:  Aún no he conseguido hacer del todo la conexión remota.

### Horas
> - Dragos :  3H
> - Adrian :  1H
> - Maria :   2H
> - Miguel :  2H
---

## Día 18/05/2020

### ¿Que hice?
>- Dragos: He cambiado los colores, he cambiado que se vacien los campos de nombre y apellido cuando se añae un punto positivo/negativo y he creado la ventana básica de Estadística vinculada al botón de estadística
>- Adrian: Buscar informaci´ón sobre añadir botones de atr´ás y siguiente para cada interfaza de la aplicación.Solucionar problema con el Window Builder.
>- Miguel: Empezar a hacer la web
>- Maria: Instalarme el ssh en linux.

### ¿Que voy hacer?
>- Dragos: Trabajar en la ventana de estadística, y en las funciones lógicas.
>- Adrian: Añadir botón de atrás para cada interfaz.
>- Miguel:  Seguir haciendo la web.
>- Maria:  Ponerme a configurar la base de datos remota con el ssh.


### ¿Qué problemas he tenido?
>- Dragos: Ninguno.
>- Adrian: No me funciona el Windows Builder a la hora de hacer el diseño de los botones, me lleva más tiempo y más dificultades..
>- Miguel: Gasto de tiempo en aparender a usar las herramientas.
>- Maria:  Nninguno.

### Horas
> - Dragos : 1H
> - Adrian : 2H
> - Maria :  1H
> - Miguel : 2H
---

## Día 20/05/2020

### ¿Que hice?
>- Dragos: He probado distintos servicios de mysql remoto.
>- Adrian: Añadir botón de atrás a cada interfaz. 
>- Miguel: Incremento de la página web.
>- Maria: Instalarme nmap y ssh, y configurar en la terminal nano etc/mysql/my.cnf la ip para hacerla remota.

### ¿Que voy hacer?
>- Dragos: Las consultas restantes para la estadística y los métodos que faltan.
>- Adrian: Editar botón de atrás y añadirle la acción de volver a la interfaz de inicio.
>- Miguel: Terminae el manual de uso y el AboutUs de la página web.
>- Maria: Volver a borrar todo lo que he hecho en mi ordenador.


### ¿Qué problemas he tenido?
>- Dragos: No he conseguido ninguno útil, los que son de tipo remoto son de pago.
>- Adrian: No me dejaba configurarlo con el Windows Builder.
>- Miguel: Faltan Ventanas en la aplicación, por lo tanto no puedo explicar para que sirven al no disponer de las imágenes.
>- Maria:   porque no entendí el concepto que me asignaron en este Spring y pensé que tenía que hacer una base de datos remota, y no, lo que tengo que hacer es burcar un host remoto.

### Horas
> - Dragos : 1H
> - Adrian : 2H
> - Maria :  2H
> - Miguel : 2H
---

## Día 22/05/2020

### ¿Que hice?
>- Dragos: He descansado, necesitaba desconectar.
>- Adrian: Terminar de editar los botones de atrás.
>- Miguel: Descanso.
>- Maria: Eliminar todas las configuraciones que hice con el nmap y en la ruta /etc/mysql/my.cnf.

### ¿Que voy hacer?
>- Dragos: Los métodos que faltan.
>- Adrian: Añadir eventos para cada vez que se habrá una clase se cierre la anterior.
>- Miguel: Descanso
>- Maria: Volver a investigar sobre los Host remotos y como hacerlos.


### ¿Qué problemas he tenido?
>- Dragos: Estrés y cansancio.
>- Adrian: Ninguno.
>- Miguel: Cansancio.
>- Maria:  Ninguno.

### Horas
> - Dragos : 0H
> - Adrian : 1H
> - Maria :  1H
> - Miguel : 0H
---

## Día 25/05/2020

### ¿Que hice?
>- Dragos: Añadir el evento a todos los botones de 'atrás'.
>- Adrian: Añadir eventos para cerrar la clase anterior.
>- Miguel: Gestión del tema del logo.
>- Maria: Estuve mirando y informandome con mis compañeros en como hacer un host remoto.

### ¿Que voy hacer?
>- Dragos: La lógica de estadística.
>- Adrian: Terminar la ventana.
>- Miguel: Investigar sobre como hacer el About Us.
>- Maria:  Propuse hacerlo en mi pc con nat al router.


### ¿Qué problemas he tenido?
>- Dragos: Ninguno.
>- Adrian: Ninguno.
>- Miguel: Ninguno.
>- Maria:  No lo conseguí, entonces hemos optado por no añadirlo.

### Horas
> - Dragos : 1H
> - Adrian : 1H
> - Maria :  2H
> - Miguel : 1H
---

## Día 27/05/2020

### ¿Que hice?
>- Dragos: He intentado hacer NAT  en el router a mi máquina para terminar el acceso a la bbdd.
>- Adrian: Terminar la ventana.
>- Miguel: Investigar sobre como hacer el About Us.
>- Maria:  Propuse hacer el host remote en mi pc con nat al router.

### ¿Que voy hacer?
>- Dragos: Intentar terminarlo y que  funcione.
>- Adrian: La presentación para exponer nuestro proyecto con el Scrum Master que es María.
>- Miguel: Terminar el About Us de la página.
>- Maria: La presentación para exponer nuestro proyecto con el Product Owner que es Adrian.


### ¿Qué problemas he tenido?
>- Dragos: La configuración que permite el router es muy básica y es complicado hacerlo.
>- Adrian: 
>- Miguel: Ninguno.
>- Maria:  Ninguno.

### Horas
> - Dragos : 2H
> - Adrian : 2H
> - Maria :  2H
> - Miguel : 1H
---

